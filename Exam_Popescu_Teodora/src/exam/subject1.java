package exam;

import java.util.ArrayList;

public class subject1 {
    public interface I{}        //Is an interface

    public class B implements I{
        private long t;
        public D d;

        public void b(){}
    }

    public class C{             //Dependency relationship with B

        public void metB(B b){}
    }

    public class D{               //Association with B
        public ArrayList<G> gs=new ArrayList<>();
        public ArrayList<F> fs=new ArrayList<>();

        public void met1(int i){}
    }

    public class E{                 //Composition with D
        public D d;

        E(){
           d=new D();
        }

        public void met1(){}
    }

    public class G{                   //Aggregation with D

        public double met3(){
            return 0;
        }
    }

    public class F{                     //Aggregation with D

        public void n(String s){}
    }
}
