package exam;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class subject2 extends JFrame{
    JLabel label1,label2;
    JTextField textBox1,textBox2;
    JButton button;

    subject2(){

        setTitle("Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setSize(300, 300);
        init();
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        int width=100;
        int height = 20;

        label1 = new JLabel("First text field:");
        label1.setBounds(10, 50, width, height);

        label2 = new JLabel("Second text field:");
        label2.setBounds(10, 100,width, height);

        textBox1 = new JTextField();
        textBox1.setBounds(150,50,width, height);

        textBox2 = new JTextField();
        textBox2.setBounds(150,100,width, height);
        textBox2.setEditable(false);

        button = new JButton("Press me");
        button.setBounds(10,150,width, height);
        button.addActionListener(new TratareButon());

        add(label1);add(label2);add(textBox1);add(textBox2);
        add(button);

    }

    public static void main(String[] args) {
        new subject2();
    }


    public class TratareButon implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text1=subject2.this.textBox1.getText();
            textBox2.setText(text1);
        }
    }
}
